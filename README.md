#Ruby Coding Exercise
###Recursive Method

Using the first and drop methods of Array. The function is written with the assumption that all elements are Numbers.
The unit tests are done for an empty array, an array with one element, array with multiple elements also with the assumption that all elements are numbers.

###Unit Test map()

Redefine the map function by adding yield to be able to unit test. The unit testing was done on an empty array and multiple elements array. The unit test was made using numerical elements as well.