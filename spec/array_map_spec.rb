require 'array_map'


RSpec.describe Array do
  context "With empty array" do
    it "returns the empty array" do
      arr = []
      arr2 = arr.map{|e| e + 1}
      expect(arr2).to eq []
    end
  end

  context "With number elements array" do
    it "returns an array with numbers +1" do
      arr = [1,2,3,4]
      arr2 = arr.map{|e| e + 1}
      expect(arr2).to eq [2,3,4,5]
    end
  end

end
