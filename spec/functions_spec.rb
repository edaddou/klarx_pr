require 'functions'

RSpec.describe SumArray do
  context "With empty array" do
    it "sums the empty array" do
      sum = SumArray.sum([])
      expect(sum).to eq 0
    end
  end

  context "With one element" do
    it "sums the one element" do
      sum = SumArray.sum([5])
      expect(sum).to eq 5
    end
  end

  context "With many elements" do
    it "sums many element element" do
      sum = SumArray.sum([5,10,-5,2.6,0b0])
      expect(sum).to eq 12.6
    end
  end
end
