#
class SumArray
  def self.sum(data)
    if data.empty? then
      return 0
    else
      return data.first + sum(data.drop(1))
    end
  end
end
