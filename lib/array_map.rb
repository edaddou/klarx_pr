#reopen the class

Array.class_eval do
  #new method
  def map
    out = []
      if block_given?
        self.each { |e| out << yield(e) }
      end
      out
  end
end
